<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends CI_Model {
	private $table = "barang";
	public $nama_barang;
	public $harga;
	public $stok;
    public $owner;
    public function rules()
    {
        return [
            ['field' => 'nameBarang',
            'label' => 'Name',
            'rules' => 'required'],

            ['field' => 'hargaBarang',
            'label' => 'Price',
            'rules' => 'numeric'],
            
            ['field' => 'stokBarang',
            'label' => 'Stock',
            'rules' => 'required']
        ];
    }
	public function getAll($nama)
    {
        $this->db->where('owner', $nama);
        return $this->db->get($this->table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_barang" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->owner = $post["owner"];
        $this->nama_barang = htmlspecialchars($post["namaBarang"]);
        $this->harga = htmlspecialchars($post["hargaBarang"]);
        $this->stok = htmlspecialchars($post["stokBarang"]);
        $this->db->insert($this->table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_barang = $post["id"];
        $this->owner = htmlspecialchars($post["owner"]);
        $this->nama_barang = htmlspecialchars($post["namaBarang"]);
        $this->harga = htmlspecialchars($post["hargaBarang"]);
        $this->stok = htmlspecialchars($post["stokBarang"]);
        $this->db->update($this->table, $this, array('id_barang' => $post['id']));
    }

    public function delete($id)
    {
        // $this->deleteImage($id);
        return $this->db->delete($this->table, array("id_barang" => $id));
    }
    

}

/* End of file M_Product.php */
/* Location: ./application/models/M_Product.php */ ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {
	private $table = "admin";
	public $username;
	public $password;
	public $first_name;
	public $last_name;
	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}
	public function save()
    {
        $post = $this->input->post();
        $this->first_name = htmlspecialchars($post["firstname"]);
        $this->last_name = htmlspecialchars($post["lastname"]);
        $this->username = htmlspecialchars($post["username"]);
        $this->password = htmlspecialchars(md5($post['password']));
        $this->db->insert($this->table, $this);
    }	
    public function getById($where)
    {
        return $this->db->get_where($this->table, ["username" => $where])->row();
    }
    // public function update(){
    // 	$post = $this->input->post();
    //     $this->id_user = $post["id"];
    //     $this->username = $post["username"];
    //     $this->first_name = $post["firstname"];
    //     $this->last_name = $post["lastname"];
    //     $this->db->update($this->table, $this, array('id_user' => $post['id']));
    // }
 
}

/* End of file M_login.php */
/* Location: ./application/models/M_login.php */ ?>
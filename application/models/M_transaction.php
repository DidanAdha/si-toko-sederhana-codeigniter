<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_transaction extends CI_Model {
	private $table = "transaksi";
    public $id_barang;
    public $admin;
	public $jumlah;
	public $harga;

	public function getAll()
    {
        return $this->db->get($this->table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id_barang" => $id])->row();
    }
    public function save()
    {
        $post = $this->input->post();
        $this->id_barang = $post["id_barang"];
        $this->admin = $post["admin_aktif"];
        $this->jumlah = htmlspecialchars($post["jumlah"]);
        $this->harga = $post["totalHarga"];
        $this->db->insert($this->table, $this);
    }

}

/* End of file M_transaction.php */
/* Location: ./application/models/M_transaction.php */
<?php 
    if ($this->session->userdata('nama')=='') {
        redirect(base_url('c_login'));
    }
    else {}
 ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
          <?php $this->load->view("partial/navbar.php") ?>


          <div class="content">
            <div class="container-fluid">
                <div class="card">
                            <div class="header">
                                <h4 class="title">Tambah Barang</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="<?php echo base_url("C_product/add") ?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Barang </label>
                                                <input type="text" name="namaBarang" class="form-control"  placeholder="Nama" required value="" autocomplete="off"> 
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="owner" value="<?php echo $this->session->userdata('nama'); ?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Harga Barang</label>
                                                <input type="number" name="hargaBarang" class="form-control" placeholder="Harga" value="" required autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Stok Barang</label>
                                                <input type="number" class="form-control" placeholder="Stok" 
                                                name="stokBarang" value="" required autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                     <!-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Gambar</label>
                                                <input type="file" class="form-control" 
                                                name="filefoto" value="" required autocomplete="off">
                                            </div>
                                        </div>
                                    </div> -->

                                    <button type="submit" class="btn btn-primary btn-fill pull-right">Save</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>


        <?php $this->load->view("partial/footer.php") ?>

    </div>
</div>


</body>

<?php $this->load->view("partial/js.php") ?>

</html>
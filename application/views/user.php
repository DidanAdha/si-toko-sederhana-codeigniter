<?php 
if ($this->session->userdata('nama')=='') {
    redirect(base_url('c_login'));
}
else {}
   ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
          <?php $this->load->view("partial/navbar.php") ?>


          <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                 <a href="#">
                                    <img class="avatar border-gray" src="<?php echo base_url('assets/img/faces/face-1.jpg') ?>" alt="..."/>

                                    <h4 class="title" id="name">
                                        <?php echo $this->session->userdata('nama'); ?>
                                    </h4>
                                </a>
                            </div>

                        </div>
                        <hr>
                        <div class="text-center">
                            <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                            <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                            <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Profile</h4>
                        </div>
                        <div class="content">
                            <form id="myForm" method="post" action="">
                                <div class="row">
                                    <div class="col-md-9">
                                        <!-- <input type="hidden" name="id" value="<?php echo $this->session->userdata('id'); ?>" readonly>
 -->                                        <div class="form-group">
                                            <label>Company</label>
                                            <input type="text" class="form-control" disabled placeholder="Company" value="Us.Soft">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" required name="username" class="form-control" placeholder="Username" value="<?php echo $this->session->userdata('nama'); ?>" readonly>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" required name="firstname" id="first" class="form-control" placeholder="First Name" value="<?php echo $data->first_name ;?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" required name="lastname" id="last" class="form-control" placeholder="Last Name" value="<?php echo $data->last_name; ?>" readonly>
                                        </div>
                                    </div>
                                </div>



                                <!-- <button onclick="myFunction()" type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button> -->
                                <div class="clearfix"></div>
                            </form >
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <?php $this->load->view("partial/footer.php") ?>

</div>
</div>


</body>

<?php $this->load->view("partial/js.php") ?>

</html>

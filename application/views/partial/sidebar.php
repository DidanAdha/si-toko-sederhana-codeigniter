<div class="sidebar" data-color="default" data-image="<?php echo base_url('assets/img/sidebar-5.jpg') ?>">

    <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://localhost/toko" class="simple-text">
                    Toko
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('c_product/') ?>">
                        <i class="pe-7s-note2"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('welcome/user') ?>">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('Welcome/new') ?>">
                        <i class="pe-7s-plus"></i>
                        <p>Add</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('c_transaction') ?>">
                        <i class="pe-7s-timer"></i>
                        <p>Sale History</p>
                    </a>
                </li>
                
				<li class="active-pro">
                    <a href="http://localhost/Us.Soft/index.php" target="_blank">
                        <i class="pe-7s-rocket"></i>
                        <p>Us.Soft</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">User</a>
                </div>
                <div class="collapse navbar-collapse">
                    <!-- <ul class="nav navbar-nav navbar-left">
                        <li>
                            <input id="myInput" type="text" name="search">
                        </li>
                    </ul> -->

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="<?php echo base_url('welcome/user') ?>">
                               <p><?php echo $this->session->userdata('nama'); ?></p>
                            </a>
                        </li>
                        
                        <li>
                             <a href="<?php echo base_url('c_login/logout') ?>">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>
<?php 
  if ($this->session->userdata('nama') != '') {
    redirect(base_url('c_product'));
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>Sign In</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" method="post" action="<?php echo base_url('c_login/aksi_login'); ?>" >
      <img class="mb-4" src="assets/brand/ussoft.png" alt="" width="200" height="150">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="username" class="sr-only">Username</label>
      <input name="username" type="text" id="username" class="form-control" placeholder="Username" required autofocus autocomplete="off">
      <label for="password" class="sr-only">Password</label>
      <input name="password" type="password" id="password" class="form-control" placeholder="Password" required autocomplete="off">
      <!-- <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div> -->
      <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Knock Knock</button>
      <br><br>
      <a href="<?php echo base_url('c_regis') ?>" class="mt-5 mb-3 text-muted">You know us?</a>
      <p class="mt-5 mb-3 text-muted">&copy; Us.Soft Indonesia</p>
    </form>
  </body>
</html>

<?php 
if ($this->session->userdata('nama')=='') {
    redirect(base_url('c_login'));
}
else {}
 ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
          <?php $this->load->view("partial/navbar.php") ?>


          <div class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Tambah Barang</h4>
                    </div>
                    <div class="content">
                        <form id="form" method="post" action="<?php echo base_url("C_transaction/save") ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Id Barang </label>
                                        <input type="text" name="id_barang" class="form-control"  placeholder="" required value="<?php echo $product->id_barang ?>" readonly >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Operator </label>
                                        <input type="text" name="admin_aktif" class="form-control"  placeholder="" required value="<?php echo $this->session->userdata('nama'); ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Barang </label>
                                        <input type="text" name="namaBarang" class="form-control"  placeholder="" value="<?php echo $product->nama_barang ?>" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Harga Satuan </label>
                                        <input type="number" id="harga" name="hargaBarang" class="form-control"  placeholder="" value="<?php echo $product->harga ?>" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Jumlah </label>
                                        <input type="number" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah Barang" value="" required onkeyup="count()">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Total </label>
                                        <input type="number" id="total" class="form-control" placeholder="" 
                                        name="totalHarga" id="harga" value="" required readonly>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-fill pull-right">Save</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div>
</div>
<script type="text/javascript">
    function count(){
        var textValue1 = document.getElementById('harga').value;
        var textValue2 = document.getElementById('jumlah').value;

        if($.trim(textValue1) != '' && $.trim(textValue2) != ''){
         document.getElementById('total').value = textValue1 * textValue2; 
     }
 }
</script>


</body>

<?php $this->load->view("partial/js.php") ?>

</html>
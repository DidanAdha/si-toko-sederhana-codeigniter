<?php 
if ($this->session->userdata('nama')=='') {
    redirect(base_url('c_login'));
}
else {}
   ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
           <?php $this->load->view("partial/navbar.php") ?>


           <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Data Barang</h4>
                                <!-- <p class="category">Here is a subtitle for this table</p> -->
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped text-center">
                                    <thead>
                                        <tr>
                                            <th><center>Id</center></th>
                                            <th><center>id_barang</center></th>
                                            <th><center>Admin</center></th>
                                            <th><center>jumlah</center></th>
                                            <th><center>total</center></th>
                                            <th><center>Waktu pembelian</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data as $key => $data): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $data->id ?>
                                                </td>
                                                <td>
                                                    <?php echo $data->id_barang ?>
                                                </td>
                                                <td>
                                                    <?php echo $data->admin ?>
                                                </td>
                                                <td>
                                                    <?php echo $data->jumlah ?>
                                                </td>
                                                <td>
                                                    Rp.
                                                    <?php echo number_format($data->harga) ?>
                                                    ,-
                                                </td>
                                                <td width="300">
                                                    <?php echo $data->time ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                </div>
            </div>




        </div>
    </div>
    <?php $this->load->view("partial/footer.php") ?>
</div>

<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
  </div>
  <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
  <div class="modal-footer">
    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
</div>
</div>
</div>
</div>
</body>

<?php $this->load->view("partial/js.php") ?>

<script type="text/javascript">
   $(document).ready(function(){

       demo.initChartist();

       $.notify({
           icon: 'pe-7s-user',
           message: "Welcome, <b>Enjoy your HomePage</b>  -Us.Soft" 

       },{
        type: 'info',
        timer: 4000
    });

   });

</script>
<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

</html>

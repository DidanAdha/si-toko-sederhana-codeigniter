<?php 
    if ($this->session->userdata('nama')=='') {
        redirect(base_url('c_login'));
    }
    else {}
 ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
          <?php $this->load->view("partial/navbar.php") ?>


          <div class="content">
            <div class="container-fluid">
                <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Barang</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="<?php echo base_url('c_product/update/'.$product->id_barang) ?>">
                                    <input type="hidden" name="id" value="<?php echo $product->id_barang ?>">
                                    <input type="hidden" name="owner" value="<?php echo $this->session->userdata('nama'); ?> ">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Barang </label>
                                                <input type="text" name="namaBarang" class="form-control"  placeholder="Nama" required value="<?php echo $product->nama_barang ?>">
                                                <div class="invalid-feedback"><?php echo form_error('namaBarang') ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Harga Barang</label>
                                                <input type="number" name="hargaBarang" class="form-control" placeholder="Harga" value="<?php echo $product->harga ?>" required>
                                                <div class="invalid-feedback"><?php echo form_error('hargaBarang') ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Stok Barang</label>
                                                <input type="number" class="form-control" placeholder="Home Address" 
                                                name="stokBarang" value="<?php echo $product->stok ?>" required>
                                            </div>
                                            <div class="invalid-feedback"><?php echo form_error('stokBarang') ?></div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-fill pull-right">Update Data</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>


        <?php $this->load->view("partial/footer.php") ?>

    </div>
</div>


</body>

<?php $this->load->view("partial/js.php") ?>

</html>
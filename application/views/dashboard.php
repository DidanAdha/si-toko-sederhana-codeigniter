<?php 
if ($this->session->userdata('nama')=='') {
    redirect(base_url('c_login'));
}
else {}
 ?>
<?php $this->load->view("partial/head.php") ?>
<body>

    <div class="wrapper">
        <?php $this->load->view("partial/sidebar.php") ?>

        <div class="main-panel">
         <?php $this->load->view("partial/navbar.php") ?>


         <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <input class="form-control" type="text" id="myInput" onkeyup="search()" placeholder="Search for names..">
                            </div>
                            
                        </div>
                        <br>
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Data Barang</h4>
                                <!-- <p class="category">Here is a subtitle for this table</p> -->
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="dataTable" class="table table-hover table-striped text-center">
                                    <thead>
                                        <tr>
                                            <th><center>Id</center></th>
                                            <th><center>Name</center></th>
                                            <th><center>Price</center></th>
                                            <th><center>Stock</center></th>
                                            <th><center>Action</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($products as $key => $product): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $product->id_barang ?>
                                                </td>
                                                <td width="400">
                                                    <?php echo $product->nama_barang ?>
                                                </td>
                                                <td>
                                                    Rp.
                                                    <?php echo number_format($product->harga) ?>
                                                    ,-
                                                </td>
                                                <td>
                                                    <?php echo $product->stok ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url('c_product/edit/'.$product->id_barang) ?>"
                                                     class="btn btn-small btn-primary"><i class="pe-7s-tools"></i> Edit</a>
                                                     <a  onclick="deleteConfirm('<?php echo base_url('c_product/delete/').$product->id_barang ?>')"
                                                         class="btn btn-small btn-danger"><i class="pe-7s-trash"></i> Delete</a>
                                                         <a href="<?php echo base_url('c_transaction/add/'.$product->id_barang) ?>"
                                                             class="btn btn-small btn-dark"><i class="pe-7s-cart"></i>Transaction</a>
                                                         </td>
                                                     </tr>
                                                 <?php endforeach; ?>

                                             </tbody>
                                         </table>

                                     </div>
                                 </div>
                             </div>


                         </div>
                     </div>




                 </div>
             </div>
             <?php $this->load->view("partial/footer.php") ?>
         </div>

         <!-- Logout Delete Confirmation-->
         <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
        </div>
    </div>
</div>
</div>
</body>

<?php $this->load->view("partial/js.php") ?>
<script type="text/javascript">
    $(document).ready(function){
        $("#myInput").on("keyup", function());
        $("#myTable tr").filter(function(){
            $(this).toogle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    }
</script>
<script type="text/javascript">
 $(document).ready(function(){

     demo.initChartist();

     $.notify({
         icon: 'pe-7s-user',
         message: "Welcome, <b>Enjoy your HomePage</b>  -Us.Soft" 

     },{
        type: 'info',
        timer: 4000
    });

 });

</script>
<script>
function search() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>
<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

</html>

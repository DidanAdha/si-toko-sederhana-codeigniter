<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class C_product extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('m_product');
	}
	
	public function index()
	{
		$data['products'] = $this->m_product->getAll($this->session->userdata('nama'));
		$this->load->view('dashboard', $data);
	}
	public function add()
	{
		$product = $this->m_product;
		$product->save();
		$data['products'] = $this->m_product->getAll($this->session->userdata('nama'));
		$this->load->view('dashboard', $data);

	}
	public function delete($id=null)
	{
		if (!isset($id)) show_404();

		if ($this->m_product->delete($id)) {
			redirect(base_url('c_product'));
		}
	}
	public function edit($id=null)
    {
        if (!isset($id)) redirect('c_product');
       
        $product = $this->m_product;
        $validation = $this->form_validation;
        $validation->set_rules($product->rules());

        if ($validation->run()) {
        	$this->load->view('errors/html/error_404');
            $product->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(base_url('c_product'));
        }

        $data["product"] = $product->getById($id);
        if (!$data["product"]) show_404();
        $this->load->view("v_edit", $data);
    }
    public function update($id=null)
    {
    	$update = $this->m_product;
    	$update->update();
    	redirect(base_url('c_product'));
    }
	
}

/* End of file C_product.php */
/* Location: ./application/controllers/C_product.php */
?>
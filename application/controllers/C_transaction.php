<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_transaction extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('m_transaction');
		$this->load->model('m_product');
	}
	public function index()
	{
		$data['data'] = $this->m_transaction->getAll();
		$this->load->view('history', $data);
	}
	public function add($id){
		if (!isset($id)) redirect('c_product');
		$data['product'] = $this->m_product->getById($id);
		if (!$data["product"]) show_404();
		$this->load->view('v_transaction', $data);
	}
	public function save(){
		$add = $this->m_transaction;
		$add->save();
		
		redirect(base_url(''));

	}

}

/* End of file C_transaction.php */
/* Location: ./application/controllers/C_transaction.php */  
?>
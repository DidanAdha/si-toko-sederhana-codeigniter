<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_regis extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
		$this->load->library('session','form_validation');

	}	
	public function index(){
		$this->load->view('v_regis');
	}
	function add(){
		$user = $this->m_login;
		$user->save();
		redirect(base_url("c_login"));
		
	}
}

/* End of file C_regis.php */
/* Location: ./application/controllers/C_regis.php */ ?>
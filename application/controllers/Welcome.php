<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
		$this->load->library('session','form_validation');

	}	

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('dashboard');
	}
	public function user(){
		$where = $this->session->userdata('nama');
		$data['data'] = $this->m_login->getById($where);
		if (!$data['data']) {
			$this->load->view('errors/hmtl/error_404');
		}
		$this->load->view('user', $data);
	}
	public function new(){
		$this->load->view('v_add');
	}
}
